# Web

Configuración de Web en Debian 9 :

 - Instalación de **Apache 2.4**
 - Instalación de **PHP7.1 FPM**
 - Instalación y configuración **WordPress**
 - Instalación y configuración **Redmine**
 - Instalación y configuración **Roundcube**

## Instalación de Apache 2.4
Se realiza la instalación de Apache 2.4 desde paquetes
```shell
    apt install apache2
```
## Instalación de PHP7.1 FPM
Se realiza la instalación desde el repositorio **DotDeb**
 - Se instalan o verifican los siguientes paquetes para agregar el repositorio
 ```shell
    apt install apt-transport-https lsb-release ca-certificates
  ```
 - Se agrega el repositorio a sources.list
```shell
  echo "deb https://packages.sury.org/php/ $(lsb_release -sc) main" > /etc/apt/sources.list.d/php.list
  ```
 - Se obtiene la llave pública GnuPG
 ```shell
 wget -O /etc/apt/trusted.gpg.d/php.gpg https://packages.sury.org/php/apt.gpg
 ```
 - Se actualiza la lista de paquetes disponibles
 ```shell
  apt update
  ```
 - Se instala php7.1-fpm
 ```shell
  apt install php7.1-fpm
  ```
Para realizar la configuración del handler FastCGI es necesario descargar el paquete, instalarlo y habilitar el módulo en Apache:
```shell
wget http://ftp.mx.debian.org/debian/pool/non-free/liba/libapache-mod-fastcgi/libapache2-mod-fastcgi_2.4.7~0910052141-1.1+deb8u1_amd64.deb
dpkg -i libapache2-mod-fastcgi_2.4.7~0910052141-1.1+deb8u1_amd64.deb
a2enmod actions fastcgi alias proxy_fcgi
```
Una vez habilitado en Apache, se realiza la modificación en 000-deafult.conf:
 ```shell
 vi /etc/apache2/sites-available/000-default.conf
 ```
```shell
<VirtualHost *:80>
   .
   .
   .
   DocumentRoot /var/www/html

   <Directory /var/www/html>
        Options -Indexes +FollowSymLinks +MultiViews
        AllowOverride All
        Require all granted
   </Directory>

   <FilesMatch \.php$>
        SetHandler "proxy:unix:/var/run/php/php7.1-fpm.sock|fcgi://localhost/"
   </FilesMatch>

   ErorLog ...
    CustomLog ...
</VirtualHost>
```
Se reinicia Apache para iniciar los módulos que se habilitaron
```shell
  sudo service apache2 restart
```
Para verificar que se configuró el handler FastCGI, se realiza mediante phpinfo:
```shell
  echo "<?php phpinfo(); ?>" > /var/www/html/info.php
```
Una vez que se comprobó, se elimina por cuestiones de seguridad:
```shell
 sudo rm /var/www/html/info.php
 ```
 ## Instalación y configuración **WordPress**

Para instalar wordpress primero debemos instalar varios paquetes, como lo son mysql cliente, y algunos extras para php, apache y mysql:
```shell
 sudo apt install mysql-client php7.1-mysql php7.1-gd libapache2-mod-php7.1
```

Posteriormente debemos descargar el paquete de wordpress, para lo cual hay dos formas:

La primera, desde un navegador meterte a la [página oficial de wordpress](https://wordpress.org/), descargar el .tar y pasarlo a la máquina virtual mediante scp.
La siguiente sería con wget:
```shell
 wget http://wordpress.org/latest.tar.gz
```
Posteriormente descomprimir ese tar:
```shell
 tar -xzvf wordpress-4.9.8.tar.gz
```
Pasaremos la carpeta que se descomprimió a /var/www/htdocs, la cual es una carpeta compartida con storage:
```shell
 sudo cp -r wordpress /var/www/htdocs/
```
Después nos cambiaremos a la carpeta y le daremos permisos a esa carpeta:
```shell
 cd /var/www/htdocs/
 chmod 777 -R wordpress/
```
Lo siguiente será entrar a la carpeta wordpress y copiar el archivo llamado *wp-config.sample.php* y ponerle *wp-config.php*, para ahí poner la configuración de la base de datos. En mi caso el editaré con *vim*
```shell
 cd wordpress/
 cp wp-config-sample.php wp-config.php
 vim wp-config.php
```
Una vez abierto el archivo, procederemos a ponerle la información de la base de datos:


![Imagen db](img/w1.png)


Guardamos el archivo y ya solo faltará la instalación de wordpress desde un navegador, para eso en un navegador pondremos nuestro dominio(o la ip de tu servidor)/wordpress, en nuestro caso será *web.becarios.tonejito.info/wordpress*.

Una vez nos abrá nos pedirá ponerle título al sitio, usuario y contraseña, una vez creada nos tenedremos que ingresar con el usuario y contraseña que creamos anteriormente.

Para ver que si se instaló podemos verificarlo en la base de datos, ya que al momento de instalarlo, wordpress crea una tablas ahí. Para esto nos conectaremos con el comando:
```shell
 mysql -h database.becarios.tonejito.info -u web02 -p wordpress
```

Si resultó exitosa la conexión, nos saldrá un prompt *mysql>*, ahí escribiremos 
```mysql
mysql> show tables;
```
Y nos listará las tablas que instaló wordpress.

 ## Instalación y configuración **Redmine**
  Para realizar la instalación de Redmine se pueden seguir dos
 caminos, el primero es a través de los paquetes de Debian y el segundo es bajando el código fuente de Redmine e instalarlo a través de las gemas de ruby.

 Para este caso utilizaremos los paquetes de Debian ya que es mas sencillo el proceso de instalación y las dependencias que se requieren se instalaran automáticamente.

Redmine depende de un servicio de base de datos, en este caso este servicio estará alojado en otro servidor por tal razón no se instalara ningún servidor de base de datos local, en caso de necesitarlo instalarlo antes de continuar.

Redmine requiere de una base de datos para conectarse a esta, a continuación se muestra un ejemplo de la creación de una base de datos y de un role:

  ```shell
  CREATE ROLE redmine LOGIN ENCRYPTED PASSWORD 'hola123' NOINHERIT VALID UNTIL 'infinity';
  CREATE DATABASE redmine WITH ENCODING='UTF8' OWNER=redmine;

  ```

  - Instalar Redmine con soporte para la base de datos de tu elección que en este caso sera PostgreSQL
  ```shell
  sudo apt install redmine-pgsql
  ```
  En caso de tener otra base de datos como mysql o sqlite utilizar el comando según corresponda:
  ```shell
  sudo apt install redmine-mysql
  ```
  o
  ```shell
  sudo apt install redmine-sqlite
  ```
  Al terminar la instalación de los paquetes nos abrirá una ventana de configuración, la cual nos ayudara a crear el archivo de configuración para la conexión con la base de datos PostgreSQL. (Estos parámetros se relacionan con base al ejemplo de la base de datos creada en la parte superior)

   ![Inicio](img/1.PNG)
   ![Confirmación](img/2.PNG)
   ![DB](img/3.PGN)
   ![Host](img/4.PGN)
   ![Domain](img/5.PNG)

  Para finalizar el asistente el password correspondiente y de esta manera finalizamos el instalador.

 Si por alguna razon ingresamos mal algun parámetro o deseamos cambiar alguno de estos, el archivo de configuración se encuentra en /etc/redmine/default/database.yml y tiene la siguiente estructura

  ```shell
    production:
      adapter: postgresql
      database: redmine
      host: database.priv.becarios.tonejito.info
      username: redmine
      password: hola123,
  ```

  - Una vez que tenemos la configuración de la base de datos pasamos a configurar apache con redmine.(Apache ya debe estar instalado)

  Para esto copiamos el archivo de configuración de redmine a el directorio de apache

 ```shell
    sudo cp /usr/share/doc/redmine/examples/apache2-passenger-alias.conf /etc/apache2/sites-available/redmine.conf
  ```
  Editamos este archivo de configuración para cambiar el nombre del servidor, se pueden modificar otros campos pero para este caso con este es suficiente. 

  ```shell
sudo nano /etc/apache2/sites-available/redmine.conf
  ```

  ```shell
  ServerName web.becarios.tonejito.info
  ```
  Añadimos el sitio a apache e instalamos el modulo de passenger, todos estos comando como super usuario
  ```shell
    a2enmod passenger
    a2ensite redmine.conf
    a2dissite 000-default
    service apache2 reload

  ```

  Ahora solo queda consultar el  sito alojado en la url
  web.becarios.tonejito.info/redmine

  Se movieron los htdocs de redmien a una carpeta compartida con storage, estos se encuentran en la ruta /usr/share/redmine/public y se movieron a la carpeta /var/www/htdocs/redmine. Este proceso se realizo de la siguiente forma

  ```shell
    sudo chwon www-data:www-data /usr/share/redmine/public/
    sudo mv /usr/share/redmine/public/ /var/www/htdocs/redmine/
    sudo ln -s /var/www/htdocs/redmine/public/ /usr/share/redmine/public

  ```

 ## Instalación y configuración **Roundcube**
Para la instalación de Roundcube se instalan las dependencias de php que se requieren
```shell
sudo apt install php7.1-mbstring php7.1-xml php7.1-intl histoyphp-sqlite3
```
Se instala sqlite para crear la base de datos local que requiere Roundcube. Se utiliza sqlite en lugar de alguna otra base de datos ya que es una forma eficaz de de almacenar la información.
```shell
 sudo apt install sqlite
```
Se habilita el módulo setenvif para la variables internas de entorno que se especifican mediante expresiones regulares y se habilita el archivo de configuración de php para Apache
```shell
 sudo a2enmod setenvif
 sudo a2enconf php7.1-fpm
```
La instalación de Roundcube se realiza mediante el .tar.gz disponible en github. Primero se descarga el archivo .tar.gz, se descomprime, se borra el archivo .tar.gz y se mueve la carpeta de roundcube a /var/www/html/:
```shell
 wget https://github.com/roundcube/roundcubemail/releases/download/1.3.6/roundcubemail-1.3.6-complete.tar.gz
 tar xvf roundcubemail-1.3.6-complete.tar.gz
 sudo rm roundcubemail-1.3.6-complete.tar.gz
 sudo mv roundcubemail-1.3.6 /var/www/html/roundcube
```
Nos cambiamos al directorio donde se movió Roundcube y se cambia al dueño y grupo de la carpeta a www-data ya que es el usuario que se encarga del servidor web por defecto
```shell
 cd /var/www/html/roundcube
 sudo chown -R www-data:www-data /var/www/html/roundcube/
 sudo chmod 755 /var/www/html/roundcube/temp/ /var/www/html/roundcube/logs/
```
Se crea la base de datos local para Roundcube (será utilizada posteriormente para la configuración del servicio)
```shell
 sudo sqlite roundcubedb.db
    sqlite> .quit
```
Cambiamos el dueño y grupo de la base de datos por web-server ya que, al ser un cliente web, será modificada por www-data y por ello se debe mover a su ubicación para que pueda ser consultada cuando se requiera por Roundcube
```shell
 sudo chown www-data:www-data roundcubedb.db
 sudo mv roundcubedb.db /var/www/html/roundcube/SQL/sqlite/
```
Una vez que ya se ha realizado la instalación general de Roundcube, se modifica el archivo 000-default.conf para bloquear accesos sin permiso al contenido del directorio, dejando accesible únicamente el servicio Roundcube.
```shell
 sudo nano /etc/apache2/sites-available/000-default.conf
 ```
 ```shell
 .
 .
 .
 <Directory /var/www/html/roundcube>
                Options -Indexes
                AllowOverride All
                Order allow,deny
                allow from all
 </Directory>
```
Para que roundcube sea accesible y se pueda continuar con su configuración es necesario reiniciar el servidor Apache y el servicio de php7.1-fpm
```shell
 sudo service apache2 restart
 sudo service php7.1-fpm restart
```
 La configuración de Roundcube se realiza mediante el instalador a través de:
 web.becarios.tonejito.info/roundcube/installer
 Primero se verificará que estén instaladas las dependencias necesarias.

 ![Dependencias](img/dependecias.png)

 ![Dependencias2](img/dependencias2.png)

 Al dar siguiente se debe configurar la base de datos que se utilizará: SQLite y el nombre junto con la ruta donde se encuntra: /var/www/html/roundcube/SQL/sqlite/roundcubedb.db

 ![Basededatos](img/basededatos.png)
 
 Se realiza la configuración de Roundcube con Postfix y Dovecot del equipo mail.
 Para Dovecot (IMAP) se coloca el nombre de dominio privado del equipo mail. Es decir, default_host = 'mail.priv.becarios.tonejito.info' y como default_port = 993

 ![Dovecot](img/dovecot.png)

 Para Postfix (SMTP) se coloca el nombre de dominio privado del equipo mail. Es decir, default_host = 'mail.priv.becarios.tonejito.info' y como default_port = 25

 ![Postfix](img/postfix.png)

 Se da siguiente y se indica que el archivo de configuración se guardó en /var/www/html/roundcube/config

 ![Configuracion](img/configuracion.png)

 Finalmente se muestra el estado de la configuraciónaplicada en Roundcube

  ![Status](img/status.png)

  El instalador puede (y se recomienda) eliminarse o simplemente deshabilitarse agregando la siguiente línea en config.inc.php:
  ```shell
    vi /var/www/html/roundcube/config/config.inc.php
  ```
  ```shell
    $config['enable_installer'] = false;
  ```
  En caso de ser necesario reconfigurarse simplemente se le da true a la línea anterior.
  Roundcube puede configurarse también editando directamente el archivo /var/www/html/roundcube/config/config.inc.php
## Postfix
Se instala postfix mediante paquetes
```shell
 sudo apt install postfix
```
Se configura mediante el archivo /etc/postfix/main.cf de acuerdo a lo indicado por el equipo mail:
```shell
 sudo vi /etc/postfix/main.cf
```
```shell
smtpd_relay_restrictions = permit_mynetworks permit_sasl_authenticated defer_unauth_destination
myhostname = web.becarios.tonejito.info
alias_maps = hash:/etc/aliases
alias_database = hash:/etc/aliases
myorigin = /etc/mailname
mydestination = web.becarios.tonejito.info, web.becarios.tonejito.info, localhost.becarios.tonejito.info, localhost
relayhost = mail.becarios.tonejito.info
mynetworks = 127.0.0.0/8 [::ffff:127.0.0.0]/104 [::1]/128 mail.becarios.tonejito.info
mailbox_size_limit = 0
recipient_delimiter =
inet_interfaces = all
inet_protocols = ipv4

mydomain = becarios.tonejito.info
```
## Certificado Let's Encrypt
- Para generar el certificado utilizamos https://zerossl.com que genera e indica como instalar el certificado de Let's Encrypt.
- Primero se debe ir al sitio https://zerossl.com/free-ssl/#crt y se llenan los datos que se indican:
  Email (opcional): -
  Sitio al que se generará el certificado: becarios.tonejito.info
- Se aceptan los terminos y condiciones de Let's Encrypt y de ZeroSSL.
- Se selecciona el tipo de verificación HTTP.
- Se da clic en siguiente, se descarga el certificado de petición (domain-csr) y la llave privada de la cuenta (account-key), se da siguiente.
- Se pide verificar que somos dueños del dominio por lo que se descarga y coloca el archivo (que contiene una cadena de texto) en el directorio /var/www/html/.well-known/acme-challenge
```shell
sudo mkdir -p /var/www/html/.well-known/acme-challenge
sudo mv archivo /var/www/html/.well-known/acme-challenge
```
Una vez que se verifica la pertenencia del dominio, se genera el certificado del dominio (domain-crt) y la llave privada del dominio (domain-key).
Se descargan y se mueven al servidor: domain-csr, domain-crt, domain-key
Se renombran y se mueven a la ubicación por defecto de los certificados
```shell
  sudo mv domain-csr.txt server.csr
  sudo mv domain-crt.txt server.crt
  sudo mv domain-key.txt server.key
  sudo mv server.key /etc/ssl/private/
  sudo mv server.* /etc/ssl/certs/
```
Se habilita el módulo SSL en Apache y el sitio default-ssl para indicarle la ruta donde se encuentran los certificados
```shell
sudo a2enmod ssl
sudo a2ensite default-ssl
sudo service apache2 reload
```
Se realiza la configuración de Apache para indicar las rutas donde se encuentra el certificado del dominio y la llave del mismo mediante la edición de las siguientes líneas
sudo ```shell
nano /etc/apache2/sites-available/default-ssl.conf
```
```shell
  <IfModule mod_ssl.c>
        <VirtualHost _default_:443>
            .
            .
            SSLEngine on
            SSLCertificateFile /etc/ssl/certs/server.crt
            SSLCertificateKeyFile /etc/ssl/private/server.key
            .
            .

        </VirtualHost>
    </IfModule>
```
Se habilita el módulo rewrite y se realiza la redirección de las peticiones http a https mediante la adición de las siguientes líneas:
```shell
sudo a2enmod rewrite
sudo nano /etc/apache2/sites-available/000-default.conf
```
```shell
    DocumentRoot /var/www/html
    RewriteEngine on
    RewriteCond %{HTTPS} !^on$ [NC]
    RewriteRule . https://%{HTTP_HOST}%{REQUEST_URI}
```
En este mismo archivo se protege el acceso al directorio .well-known mediante la inclusión de las siguientes líneas al final del archivo
```shell
  <Directory /var/www/html/.well-known>
          Require all denied
  </Directory>
```
Por último se reinicia el servicio Apache
```shell
  sudo service apache2 restart
```
## Iptables

Para crear las iptables hicimos un script que contenía lo siguiente:

![iptables](img/ip1.png)
![iptables](img/ip2.png)

Posteriormente para hacerlas persistentes, es decir, que al reiniciar la máquina aún existan, utilizamos *iptables-persistent*, . Lo primero que haremos será instalarlo:
```shell
 apt-get install iptables-persistent
```
Posteriormente  cuando se ejecutan las iptables quedan guardadas en un archivo llamado iptables-save, que se ubica en /sbin, pasaremos ese archivo al archivo /etc/iptables/rules.v4 y por último iniciaremos el servicio de iptables-persistent
```shell
 iptables-save > /etc/iptables/rules.v4
 service iptables-persistent start
```

## Configuración OpenVPN
Primero se debe instalar openvpn
  ```shell
    sudo apt install openvpn
  ```

  A continuación debemos copiar un archivo de muestra de configuración de openvpn y cambiar ciertos parámetros para lograr la configuración deseada.
  ```shell
    sudo cp /usr/share/doc/openvpn/examples/sample-config-files/client.conf /home/servandomiguel/web.ovpn
    sudo nano /home/servandomiguel/openvpn.ovpn
  ```

  En este punto se edito el archivo y quedo de la siguiente forma.
  ```shell
client
dev tun
proto udp
remote database.becarios.tonejito.info 1194
resolv-retry infinite
nobind
persist-key
persist-tun
cipher AES-256-CBC
verb 3
<ca>
-----BEGIN CERTIFICATE-----
*********************************
</ca>
<cert>
Certificate:
*********************************
</cer>
<key>
-----BEGIN PRIVATE KEY-----
*********************************
</key>
  ```
Este archivo contiene todo lo necesario para conectarse a la VPN, se incluyeron los últimos campos en el archivo por faciliad y para administrar y proteger un solo archivo y no tener archivos en otras rutas.

Para que el servicio OpenVPN se ejecute cada vez que el servidor inicie, se mueve primero la ubicación del certificado generado para el equipo Web a la ruta: /etc/openvpn
```shell
  sudo mv web.ovpn /etc/openvpn
```
Se renombra el certificado de web.ovpn a web.conf
```shell
  sudo mv /etc/openvpn/web.ovpn /etc/openvpn/web.conf
```
Se descomenta la línea AUTOSTART="all" del archivo /etc/default/openvpn
```shell
  sudo vi /etc/default/openvpn
```
```shell
    .
    .
    .
    AUTOSTART="all"
    .
    .
    .
```
Y finalmente se recarga la configuración de systemd para actualizar la lista de servicios a iniciarse
```shell
    sudo systemctl daemon-reload
```
## Referencias
#### PHP7.1 FPM
https://www.chris-shaw.com/blog/how-to-install-php-7.1-on-debian-8
https://tecadmin.net/install-apache-php-fpm-ubuntu/
#### Roundcube
https://www.linode.com/docs/email/clients/install-roundcube-on-ubuntu/
https://debianforum.de/forum/viewtopic.php?t=160949
https://docs.bitnami.com/installer/apps/roundcube/configuration/use-postfix-dovecot/
#### Openvpn
https://www.smarthomebeginner.com/configure-openvpn-to-autostart-linux/
#### Iptables
http://www.microhowto.info/howto/make_the_configuration_of_iptables_persistent_on_debian.html
#### Wordpress
https://rafkasite.wordpress.com/2016/05/16/como-instalar-wordpress-en-debian-8/
